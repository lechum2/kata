﻿using System;
using Xunit;

namespace Poker.Tests
{
    public class EvaluatorTests : IDisposable
    {
        private readonly Evaluator evaluator;

        public EvaluatorTests()
        {
            evaluator = new Evaluator();
        }

        public void Dispose()
        {
        }

        [Fact]
        public void CanCreateEvaluator()
        {
            Assert.NotNull(evaluator);
        }

        [Fact]
        public void WhenInputFromatIsNotCorrect_ShouldThrowException()
        {
            Assert.Throws<InvalidInputException>(() => evaluator.Evaluate("test"));
        }
    }
}
